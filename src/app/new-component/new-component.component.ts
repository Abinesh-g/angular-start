import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-component',
  templateUrl: './new-component.component.html',
  styleUrls: ['./new-component.component.css']
})
export class NewComponentComponent implements OnInit {
  name: string;
  age: bigint;
  isAlive = false;
  click = 'No Click Found';
  clicked = false;
  listOfNames = [];

  constructor() {
    setTimeout(() => {
      this.isAlive = true;
    }, 2000);
    }
  onClick() {
    this.listOfNames.push(this.name);
    this.clicked = true;
    this.click = 'Click Found';
  }
  ngOnInit(): void {
  }

}
