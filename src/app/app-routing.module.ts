import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewComponentComponent } from './new-component/new-component.component';
import { SecondComponent } from './second/second.component';


const routes: Routes = [
  {path : 'new', component : NewComponentComponent},
  {path: 'second', component: SecondComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  declarations: [
    SecondComponent
  ],
  exports: [RouterModule, SecondComponent]
})
export class AppRoutingModule { }
